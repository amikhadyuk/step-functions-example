const AWS = require('aws-sdk');

const stepFunctions = new AWS.StepFunctions();

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };
}

module.exports.runStepFunction = async () => {
  const params = {
    stateMachineArn: process.env.statemachine_arn,
    input: JSON.stringify({ specialData: 'id', secondsToWait: 10 }),
  };

  return stepFunctions.startExecution(params).promise();
};

module.exports.firstStep = async (event) => {
  // event = { "specialData": "id" }
  return {
    ...event,
    firstStep: new Date(),
  };
};

module.exports.lastStep = async (event) => {
  // event = { "specialData": "id", "firstStep": true }
  return {
    ...event,
    lastStep: new Date(),
  };
};
